import XMonad
import System.IO
import Data.Default
import XMonad.Util.SpawnOnce
import XMonad.Util.Run(spawnPipe)
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks


--------------------------
--  default definition  --
--------------------------
myModMask :: KeyMask
myModMask     = mod4Mask -- Win key or Super_L

myTerminal :: String
myTerminal    = "alacritty"

myBorderWidth :: Dimension
myBorderWidth = 1

--myWorkspaces = [" dev ", " www ", " sys ", " doc ", " vbox ", " chat ", " mus ", " vid ", " gfx "]
myWorkspaces  = ["1","2","3","4","5","6","7","8","9"]

myNormColor :: String       -- Border color of normal windows
myNormColor   = "#282c34"   -- Border color of normal windows

myFocusColor :: String
myFocusColor  = "#46d9ff"   -- Border color of focused windows


---------------------
--  startup hooks  --
---------------------
myStartupHook :: X ()
myStartupHook = do
    --spawnOnce "xmobar &"
    spawnOnce "xrandr --output LVDS1 --off --output DP1 --off --output HDMI1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output VGA1 --off --output VIRTUAL1 --off &"
    spawnOnce "picom &"
    spawnOnce "nitrogen --restore &"
    spawnOnce "imwheel &"


---------------------
--  main function  --
---------------------
main :: IO ()
main = do
    -- start Xmobar process
    xmproc <- spawnPipe "xmobar $HOME/.config/xmobar/xmobarrc"
    xmonad def
        { manageHook = manageDocks <+> manageHook def
        , layoutHook = avoidStruts  $  layoutHook def
        , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor "green" "" . shorten 50
                        }
        , terminal           = myTerminal
        , modMask            = myModMask
        , borderWidth        = myBorderWidth
        , startupHook        = myStartupHook
        , workspaces         = myWorkspaces
        , normalBorderColor  = myNormColor
        , focusedBorderColor = myFocusColor
        }
