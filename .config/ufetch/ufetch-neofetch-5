#!/bin/sh
#
# ufetch-neofetch - tiny system info for manjaro

## INFO

# user is already defined
hostname="$(cat /etc/hostname)"
host="$(hostnamectl | awk 'NR==9' | awk '{print $3}')"
os='Archlinux'
kernel="$(uname -sr)"
uptime="$(uptime -p | sed 's/up //')"
packages="$(pacman -Q | wc -l)"
shell="$(basename "${SHELL}")"
total="$(free -m  | grep ^Mem | tr -s ' ' | cut -d ' ' -f 2)"
used="$(free -m  | grep ^Mem | tr -s ' ' | cut -d ' ' -f 3)"
shared="$(free -m  | grep ^Mem | tr -s ' ' | cut -d ' ' -f 5)"
used2="$((used+shared))"
memorypercentage="$((used2*100/total))"
Xaxis=$(xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f1)
Yaxis=$(xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f2)
MaxRes=$(($Xaxis))"x"$(($Yaxis))
terminal="$(tput longname | awk 'NR==1' | awk '{print $1}')"
cpu="$(cat /proc/cpuinfo | grep 'model name' | uniq | awk 'NR==1' | awk '{print $4, $5, $6, $7}')"
gpu="$(lspci | grep ' VGA ' | cut -d" " -f 1 | xargs -i lspci -v -s {} | awk 'NR==1' | awk '{print $9, $1, $10}')"
server="$(echo $XDG_SESSION_TYPE)"
diskpercentage="$(df -t ext4 | awk 'NR==2' | awk '{print $5}')"
diskused="$(df -t ext4 | awk 'NR==2' | awk '{print  int($3/1000000)}')"
disktotal="$(df -t ext4 | awk 'NR==2' | awk '{print  int($2/1000000)}')"
shell_version="$(bash --version | head -1 | cut -d ' ' -f 4 | cut -c 1-6)"
terminal_ver="$($(ps -p $(ps -p $$ -o ppid=) o args=) --version)"

## UI DETECTION

parse_rcs() {
	for f in "${@}"; do
		wm="$(tail -n 1 "${f}" 2> /dev/null | cut -d ' ' -f 2)"
		[ -n "${wm}" ] && echo "${wm}" && return
	done
}

rcwm="$(parse_rcs "${HOME}/.xinitrc" "${HOME}/.xsession")"

ui='unknown'
uitype='UI'
if [ -n "${DE}" ]; then
	ui="${DE}"
	uitype='DE'
elif [ -n "${WM}" ]; then
	ui="${WM}"
	uitype='WM'
elif [ -n "${XDG_CURRENT_DESKTOP}" ]; then
	ui="${XDG_CURRENT_DESKTOP}"
	uitype='DE'
elif [ -n "${DESKTOP_SESSION}" ]; then
	ui="${DESKTOP_SESSION}"
	uitype='DE'
elif [ -n "${rcwm}" ]; then
	ui="${rcwm}"
	uitype='WM'
elif [ -n "${XDG_SESSION_TYPE}" ]; then
	ui="${XDG_SESSION_TYPE}"
fi

ui="$(basename "${ui}")"
desktop_session="$(env | grep DESKTOP_SESSION | cut -c 17-30 | tr [:upper:] [:lower:])"

## DEFINE COLORS

# probably don't change these
if [ -x "$(command -v tput)" ]; then
	bold="$(tput bold)"
	black="$(tput setaf 0)"
    grey="$(tput setaf 8)"
	red="$(tput setaf 1)"
	green="$(tput setaf 2)"
	yellow="$(tput setaf 3)"
	blue="$(tput setaf 4)"
	magenta="$(tput setaf 5)"
	cyan="$(tput setaf 6)"
	white="$(tput setaf 7)"
	reset="$(tput sgr0)"
fi

# you can change these
lc="${reset}${bold}${grey}"        # labels
nc="${reset}${bold}${grey}"        # user and hostname
ic="${reset}"                       # info
c0="${reset}${grey}"               # first color

## OUTPUT

cat <<EOF

${c0}                 +                 ${nc}${USER}${ic}@${nc}${hostname}${reset}
${c0}                +++                ${ic}-----------
${c0}               +++++               ${lc}OS: ${ic}${os}${reset}
${c0}              +++++++              ${nc}Host: ${ic}${host}${reset}
${c0}             +++++++++             ${nc}Hostname: ${ic}${hostname}${reset}
${c0}            +++++++++++            ${lc}Kernel: ${ic}${kernel}${reset}
${c0}           +++++++++++++           ${lc}Uptime: ${ic}${uptime}${reset}
${c0}          +++++++++++++++          ${lc}Packages: ${ic}${packages}${reset} (pacman)
${c0}         +++++++++++++++++         ${lc}Shell: ${ic}${shell} ${shell_version} ${reset}
${c0}        +++++++     +++++++        ${lc}${uitype}: ${ic}${ui} ${desktop_session} ${reset}
${c0}       +++++++       +++++++       ${lc}Display server: ${ic}${server}${reset}
${c0}      ++++++++       ++++++++      ${lc}Resolution: ${ic}${MaxRes}${reset}
${c0}     ++++++++++     ++++++++++     ${lc}Terminal: ${ic}${terminal}${reset}
${c0}    +++++                 +++++    ${lc}CPU: ${ic}${cpu}${reset}
${c0}   +++                       +++   ${lc}GPU: ${ic}${gpu}${reset}
${c0}  +                             +  ${lc}Memory: ${ic}${used2}${reset}MiB(${ic}${memorypercentage}${reset}%) / ${ic}${total}${reset}MiB

EOF
